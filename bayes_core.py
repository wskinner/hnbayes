#defines the classes and functions necessary for representing the data as a bayes net, and for calculating conditional probabilities
#**makes the assumption that all features are independent of all other features -- i.e. naive bayes
#features used: word in title, more comments than average, more points than average, source website
#each feature has exactly one edge: to the root

class Feature:
    
    def __init__(self, name, occurrences, selfAndGood):
        self.name = name
        self.occurrences = occurrences
        self.selfAndGood = selfAndGood

    def addOccurrence(self):
        self.occurrences += 1

    def addSelfAndGood(self):
        self.selfAndGood += 1

    #samples is an integer representing the number of articles that have been analyzed so far
    def pSelfAndGood(self, samples):
        return self.selfAndGood*1.0/samples
    
    #evidence is a list of features. returns p(self | evidence)
    #note that this node is either a parent of therootnode or therootnode itself
    def conditionalProb(self, evidence, samples):
        if evidence[0].name == 'therootnode':
            return 
        prod = 0.0



#like build, but after the user inputs a 0 or 1 display the predicted p(goodArticle)
def learnAndPredict():
    features, articles, goodArticles, sumComments, sumPoints = loadFeatures()
    hnarticles = open('hnarticledata', 'r')
    newData = []
    for line in hnarticles:
        print line + '\n'
        userInput = raw_input('If you would read the article type 1, otherwise type 0: ')
        if userInput == 'end':
            hnarticles.close()
            addFeatures(features, newData)
            return
        newData.append((int(userInput), line))
        line = line.split()
        line.pop()
        lineComments = line.pop()
        lineFeatures = []
        if int(lineComments) >= int(sumComments)/int(articles):
            lineFeatures.append(features['manycomments'])
        else:
            lineFeatures.append(features['fewcomments'])
        linePoints = line.pop()
        if int(linePoints) >= int(sumPoints)/int(articles):
            lineFeatures.append(features['manypoints'])
        else:
            lineFeatures.append(features['fewpoints'])
        for word in line:
            if word in features:
                lineFeatures.append(features[word])
        pGood = features['therootnode'].conditionalProb(lineFeatures, int(articles))
        print 'the probability of that article being good was P = ' + str(pGood) + '\n'
    hnarticles.close()
    addFeatures(features, newData)
    return

#add features to the features text file
#newData in format [(good, line)...]
def addFeatures(features, featureList):
    return

#return p(goodArticle)
#takes a line from the hnarticledata file
def predict(article, features):
    return

def featureStats():
    f = open('features', 'r')
    return 

#builds and returns a dictionary from the features text file
#returns a tuple (feaetures, articles, goodArticles, sumComments, sumPoints)
def loadFeatures():
    f = open('features', 'r')
    f.readline()
    l = f.readline().split()
    goodArticles = l.pop()
    articles = l.pop()
    sumComments = l.pop()
    sumPoints = l.pop()
    features = {}
    for line in f:
        line = line.split()
        pSelfAndGood = line.pop()
        occurrences = line.pop()
        name = line.pop()
        features[name] = Feature(name, occurrences, pSelfAndGood)
    f.close()
    return (features, articles, goodArticles, sumComments, sumPoints)

    

def predict(root, line, features, avgComments, avgPoints, standardFeatures, samples, lineFeatures = []):
    line = line.split()
    nullLine = False
    if lineFeatures == []:
        nullLine = True
    if line.pop() >= avgComments:
       lineFeatures.append(standardFeatures[0])
    else:
        lineFeatures.append(standardFeatures[1])
    if line.pop() >= avgPoints:
        lineFeatures.append(standardFeatures[2])
    else:
        lineFeatures.append(standardFeatures[3])
    source = line.pop()
    if source in features:
        lineFeatures.append(features[source])
    if nullLine:
        for word in line[1:]:
            if word in features:
                lineFeatures.append(features[word])
    return root.conditionalProb(lineFeatures, samples)

def build(filepath, oldFeatures = None):
    """
    Build the bayes net by asking user to decide whether each link is good or bad based on the features
    OldFeatures is the name of an optional text file containing features already added at an earlier time
    """
    features = {}
    articles = 0
    goodArticles = 0
    commentSum = 0
    pointSum = 0
    if oldFeatures != None:
        oldFile = open(oldFeatures, 'r')
        oldFile.readline()
        header = oldFile.readline()
        goodArticles = int(header.pop())
        articles = int(header.pop())
        commentSum = header.pop()
        pointSum = header.pop()
        for line in oldFile:
            line = line.split()
            pSelfAndGood = line.pop()
            occurrences = line.pop()
            name = line.pop()
            features[name] = Feature(name, occurrences, pSelfAndGood)
        oldFile.close()
    else:
        features['therootnode'] = Feature('therootnode', 0, 1)
        features['manycomments'] = Feature('manycomments', 0, 0)
        features['fewcomments'] = Feature('fewcomments', 0, 0)
        features['manypoints'] = Feature('manypoints', 0, 0)
    features['fewpoints'] = Feature('fewpoints', 0, 0)
    data = open(filepath, 'r')
    dataset = set(data)
    data.close()

    #precomputed sample averages
    avgComments = 30
    avgPoints = 91
    for line in dataset:
        articles += 1
        standardFeatures = [features[x] for x in ['manycomments', 'fewcomments', 'manypoints', 'fewpoints']]
        line = line.split()
        line.pop()
        comments = line.pop()
        points = line.pop()
        source = line.pop()
        title = ' '.join(line)
        print '\n' + title + "\n" + comments + " comments, " + points + " points, " + "from " + source
        good = raw_input("Enter 1 if you would read the article, else enter 0: ")
        while good not in ['1', '0', 'end']:
            good = raw_input("Enter 1 if you would read the article, else enter 0: ")
        if good == 'end':
            featureFile = open('features', 'a')
            featureFile.write("Features in format: name, occurrences, pSelfAndGood\n")
            featureFile.write(str(pointSum) + ' ' + str(commentSum) + ' ' + str(articles) + ' ' + str(goodArticles) + '\n') 
            for v in features.itervalues():
                featureFile.write(v.name + " " + str(v.occurrences) + " " + str(v.pSelfAndGood(articles)) + "\n")
            featureFile.close()
            return features
        goodArticles += int(good)
        features['therootnode'].addOccurrence()
        lineFeatures = []
        #handle the word-in-title features
        for word in title.split():
            if word not in features:
                features[word] = Feature(word, 1, 1*int(good))
            else:
                features[word].addOccurrence()
                if int(good):
                    features[word].addSelfAndGood()
            lineFeatures.append(features[word])

        #handle the point and comment features
        if int(good):
            if points >= avgPoints:
                features['manypoints'].addOccurrence()
                features['manypoints'].addSelfAndGood()
            else:
                features['fewpoints'].addOccurrence()
                features['fewpoints'].addSelfAndGood()

            if comments >= avgComments:
                features['manycomments'].addOccurrence()
                features['manycomments'].addSelfAndGood()
            else:
                features['fewcomments'].addOccurrence()
                features['fewcomments'].addSelfAndGood()
        else:
            if points >= avgPoints:
                features['manypoints'].addOccurrence()
            else:
                features['fewpoints'].addOccurrence()

            if comments >= avgComments:
                features['manycomments'].addOccurrence()
            else:
                features['fewcomments'].addOccurrence()

        #handle the source features
        if source not in features:
            features[source] = Feature(source, 1, 1*int(good))
        else:
            features[source].addOccurrence()
            if int(good): 
                features[source].addSelfAndGood()
        lineFeatures.append(features[source])

    featureFile = open('features', 'a')
    featureFile.write("Features in format: name, occurrences, pSelfAndGood\n")
    featureFile.write(str(pointSum) + ' ' + str(commentSum) + ' ' + str(articles) + ' ' + str(goodArticles) + '\n') 
    for v in features.itervalues():
        featureFile.write(v.name + " " + str(v.occurrences) + " " + str(v.pSelfAndGood(articles)) + "\n")
    featureFile.close()
    return features
