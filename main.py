from bayes_core import *
from story_parser import *
from sys import argv

if __name__ == "__main__":
    if len(argv) > 1:
        if argv[1] == 'learn':
            learnAndPredict()
        else:
            print 'invalid argument to main()'
    else:
        build('hnarticledata')
