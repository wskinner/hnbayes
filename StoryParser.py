#Collect recent posts from hacker news and store them in a text file with format like this:
#1 title source points comments 
#

from BeautifulSoup import BeautifulSoup
import mechanize, urllib2


def parse_to_file(soup, n):
    titles = [t for t in soup.findAll("td", "title") if t.find('a') != None]
    titles.pop()
    subtexts = soup.findAll("td", "subtext")
    articles = open("hnarticledata",'a')
    linenumber = 30*n + 1
    for title,subtext in zip(titles, subtexts):
        #print "title: " + str(title)
        #print "subtext: " + str(subtext)
        source = 'null'
        comments = 0
        points = 0
        t = unicode(title.find('a').text)
        t = t.encode('ascii','ignore')
        if title.find('span') != None:
            source = title.find('span').text
            source = source[1:len(source)-1]
        if subtext.find('span') != None:
            points = subtext.find('span').text.split()[0]
        if subtext.find('a') != None:
            comments = subtext.findAll('a')[1].text.split()[0]
        #print "t: " + t
        articles.write(str(linenumber)+" "+t+" "+str(source)+" "+str(points)+" "+str(comments)+"\n")
        linenumber += 1
    articles.close()

if __name__ == "__main__":
    base_url = "http://news.ycombinator.com"
    br = mechanize.Browser()
    br.open(base_url)
    i = 0
    while i < 5:
        soup = BeautifulSoup(urllib2.urlopen(br.geturl()))
        parse_to_file(soup, i)
        links = [link for link in br.links()]
        br.follow_link(links[len(links)-11])
        i += 1

