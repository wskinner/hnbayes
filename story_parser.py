#Collect recent posts from hacker news and store them in a text file with format like this:
#1 title source points comments 
#

from BeautifulSoup import BeautifulSoup
import mechanize, urllib2


def parse_to_file(soup, n):
    closedSet = set()
    try:
        closed = open('hnarticledata', 'r')
        for line in closed:
            line = line.split()
            articleID = line.pop()
            closedSet.add(articleID)
    except IOError:
        pass
    titles = [t for t in soup.findAll("td", "title") if t.find('a') != None]
    titles.pop()
    subtexts = soup.findAll("td", "subtext")
    articles = open("hnarticledata",'a')
    for title,subtext in zip(titles, subtexts):
        #print "title: " + str(title)
        #print "subtext: " + str(subtext)
        source = 'null'
        comments = 0
        points = 0
        link = 'null'
        t = unicode(title.find('a').text)
        t = t.encode('ascii','ignore')
        if title.find('span') != None:
            source = title.find('span').text
            source = source[1:len(source)-1]
        if subtext.find('span') != None:
            points = subtext.find('span').text.split()[0]
        if subtext.find('a') != None:
            comments = subtext.findAll('a')[1].text.split()[0]
            link = subtext.findAll('a')[1]['href'][8:]
            if link in closedSet:
                continue
            if comments == 'discuss':
                comments = 0
        #print "t: " + t
        articles.write(t.lower()+" "+str(source).lower()+" "+str(points).lower()+" "+str(comments).lower() + ' ' +  link + "\n")
    articles.close()

if __name__ == "__main__":
    base_url = "http://news.ycombinator.com"
    br = mechanize.Browser()
    br.open(base_url)
    i = 0
    while i < 7:
        soup = BeautifulSoup(urllib2.urlopen(br.geturl()))
        parse_to_file(soup, i)
        links = [link for link in br.links()]
        br.follow_link(links[len(links)-11])
        i += 1

